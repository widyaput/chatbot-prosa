# Backend Internship Application Assignment
> Implementation of conversational agent.

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)
* [Code Examples](#code-examples)
* [Features](#features)
* [Status](#status)
* [Contact](#contact)

## General info
This project create conversational agent using NLP (Natural Language Processing). Users can interact with chatbot through webpage. Communication between webpage and server using websocket technology.

## Technologies
* Docker
* Python
* Websocket
* FastAPI
## Setup
1. Install docker and docket-compose in computer.
Click [this](https://docs.docker.com/engine/install/) link to see installing docker guide.
Click [this](https://docs.docker.com/compose/install/) link to see installing docker-compose guide.
2. Actually you can use docker only because python will be installed inside docker container. But if you want to use python in computer, dont forget to install it from [this](https://www.python.org/downloads/) link.
3. Do git clone using terminal or shell.
```bash
git clone https://gitlab.com/widyaput/chatbot-prosa.git
```

## Code Examples
If you wanna use docker
1. Change your directory into project directory.
2. Execute this command below to build the first time.
```bash
docker-compose -f compose/docker-compose.yml -p prosa-chatbot up --build
```
After building, you can just type this command below to start container again.
```bash
docker-compose -f compose/docker-compose.yml -p prosa-chatbot up
```

3. Server will be live on port 8000.

3. If you wanna train another data, just change app/intents.json, after that just execute command below
```bash
docker exec -it ${id container app} bash
```
and then in the bash container, execute this command
```bash
python train.py
```
You can check id container app using this command below,
```bash
docker ps
```
and then choose the right container.

If you dont wanna use docker and python already installed
1. Change your directory into project directory.
2. Install virtualenv using one of these commands
```bash
pip install virtualenv
```
```bash
python3 -m pip install virtualenv
```
3. Create virtualenv
```bash
virtualenv venv
```
4. Activate environment using one of these commands
  - Windows
    - ```bash
      venv\Scripts\activate
      ```
  - Linux or Mac
    - ```bash
      source venv/bin/activate
      ```
5. And then install all requirements
```bash
pip install -r app/requirements.txt
```
6. Last, execute the server
```bash
python app/server.py
```
7. Server will be live on port 8000.

8. If you wanna train another data, just change app/intents.json, after that just execute command below
```bash
python app/train.py
```
## Features
* Can send message in real-time using websocket.
* Can train another dataset.
* Has 2 options to isolate project, using docker or virtual environment.
* To interact with chatbot, user needs to enter token. Default token is 'ibbot-chat'.

## Status
Project is: _finished_

## Contact
Created by Widya Anugrah Putra - feel free to contact me!
