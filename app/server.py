import asyncio
import uvicorn
import os

import handler

from typing import List, Optional
from fastapi.param_functions import Depends
from fastapi import FastAPI, WebSocket, WebSocketDisconnect, Request, Query, status
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

templates = Jinja2Templates(directory=os.path.join(os.path.dirname(__file__), "static"))

app = FastAPI()

app.mount("/static", StaticFiles(directory=os.path.join(os.path.dirname(__file__), "static")), name="static")

@app.get("/")
async def get(request: Request):
    return templates.TemplateResponse('index.html', {"request": request})

class ConnectionManager:
  def __init__(self) -> None:
    self.active_connections: List[WebSocket] = []
  
  async def connect(self, websocket: WebSocket):
    await websocket.accept()
    self.active_connections.append(websocket)
  
  def disconnect(self, websocket: WebSocket):
    self.active_connections.remove(websocket)

  async def send_message(self, str: str, websocket: WebSocket):
    await websocket.send_text(str)

  async def receive_message(self, websocket:WebSocket):
    data = await websocket.receive_text()
    return data

manager = ConnectionManager()

async def get_token(
  websocket: WebSocket,
  token: Optional[str] = Query("token")
):
  print(token)
  if token is None or token != 'ibbot-chat':
    await websocket.close(code = status.WS_1008_POLICY_VIOLATION)
  return token

@app.websocket("/")
async def ws_endpoint(websocket: WebSocket, token: str = Depends(get_token)):
  if token is None or token != 'ibbot-chat':
    return
  await manager.connect(websocket=websocket)
  try:
    while True:
      data = await asyncio.wait_for(manager.receive_message(websocket), timeout=60)
      await manager.send_message(handler.handle_message(data), websocket)
  except WebSocketDisconnect:
    manager.disconnect(websocket)
  except asyncio.TimeoutError:
    await manager.send_message('You have been idled for 1 minutes, goodbye', websocket)



if __name__ == '__main__' :
  uvicorn.run("server:app", port=8000, host="0.0.0.0", reload=True)