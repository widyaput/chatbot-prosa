from typing import List
import numpy as np
import nltk 

# Run this line for the first time
# nltk.download('punkt') 

from nltk.stem.porter import PorterStemmer
stemmer = PorterStemmer()

def tokenize(sentence: str) -> 'list[str]':
  """
  split sentence into array of tokens
  a token can be a word, punctuation char, or number
  """
  return nltk.word_tokenize(sentence)

def stem(word: str) -> str:
  """
  stemming is finding the root of the word
  Given words = 'Organize'
  Then stemmed word = 'organ'
  """
  return stemmer.stem(word)

def bag_of_words(tokenized_words: 'list[str]', words: List[str]) -> np.ndarray:
  """
  Return bag of words from tokenized words
  Example:
  tokenized_words = [ 'hi', 'how', 'are', 'you' ]
  words = [ 'hi', 'how', 'are', 'you', 'who']
  then the bag or words will be
  bow = [1, 1, 1, 1, 0]
  1 if exists otherwise 0
  """
  sentence_words = [stem(word) for word in tokenized_words]
  bag = np.zeros(len(words), dtype=np.float32)
  for idx, word in enumerate(words):
    if word in sentence_words:
      bag[idx] = 1
  return bag
